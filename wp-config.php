<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'coba_word' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Sf(8J^W*j<*OHt[C4^rs4f^)zN8[ePx1`Gpt>yD|IdRZP|hK2u;(xj|Y=`k`[7@a' );
define( 'SECURE_AUTH_KEY',  'Q4Onm-:1a@` rY1t/]={:tavq7Kap{jHqxbQ/_#b ]C9>Y$W1VXaO4}[<BR)7}X#' );
define( 'LOGGED_IN_KEY',    '8@y3;*o 1v,SSg^P)1Ts{@bIxQI;_BR%:BxlSSZ8]ng!pIb0IikT#H{S6b*YLaM$' );
define( 'NONCE_KEY',        'A{0 1g5dGIr3jyIPuGD-Hgq/`j3V`Uw4p_8s2yn+a cd2DpdS/v3=O>ad[W1~DD%' );
define( 'AUTH_SALT',        'wA!j0@fc{7L:N89~1t-N/c2e5l?z0yp59rl)}YGjdN~Y^QM`3n21atA?B_<i[}eq' );
define( 'SECURE_AUTH_SALT', 'iw`ZYv4(d%OCHVc9{7Wg/?#f*whtzPf<hWmR%?9ZA|dw4W94LBj~`DDJ[ z5CSFn' );
define( 'LOGGED_IN_SALT',   ' 8ukEwwW1{k/jHvEB}1!8Y>554}=5fZUb*0H/?KrKmVn[(Bq4O$9Wv,YBJ)7L9iz' );
define( 'NONCE_SALT',       '^}{OJYc!}.Ykj.^ZJeW^f3|0%/<C#h_{0;F[|gm0J h;_5i/d467qUDUX!ruq4J?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
