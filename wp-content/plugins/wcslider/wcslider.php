<?php 
/*
	Plugin Name: BxSlider for WooCommerce
	Plugin URI: http://easy-webdev.com
	Description: Adds bxslider into WooCommerce
	Version: 1.0
	Author: Sigit Budianto
	Author:
	Text domain: woocommerce
*/


// Error if someone opens this directly
defined('ABSPATH') or die("Go away!!");

// Define a Path to the Plugin
define('WCSLIDER_PATH', plugin_dir_url(__FILE__) );
 

//Load Scripts and style

function wcslider_scripts() {
	wp_enqueue_style('bxslider', WCSLIDER_PATH . '/css/jquery.bxslider.min.css');

	if(wp_script_is('jquery', 'enqueued')) {
		return;
	} else {
		wp_enqueue_script('jquery');	
	}
	wp_enqueue_script('bxsliderjs',WCSLIDER_PATH . '/js/jquery.bxslider.min.js');
}
add_action('wp_enqueue_scripts', 'wcslider_scripts');


// Create a Shortcode to display the Slider
// use: [wcslider]

function wcslider_shortcode() {
	$args = array(
		'posts_per_page' => 10,
		'post_type' => 'product',
		'meta_key' => '_thumbnail_id',
		'tax_query' => array(
					array( 
						'taxonomy'	=> 'product_visibility',
						'field'		=> 'name',
						'terms'		=> 'featured',
						'operator'	=> 'IN'
					)
				),
	);
	$slider_products = new WP_Query($args);
	echo "<ul class='slider_products'>";
	while($slider_products->have_posts()) : $slider_products->the_post(); ?>
		<li>
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('shop_catalog'); ?>
				<?php the_title('<h3>', '</h3>'); ?>
			</a>
		</li>

		<?php endwhile; wp_reset_postdata();
	echo "</ul>";
}
add_shortcode('wcslider', 'wcslider_shortcode');

// Execute bxSlider

function wcslider_execute(){ ?>

<script>
	$ = jQuery.noConflict()
	$(document).ready(function(){
		$('.slider_products').bxSlider({
			auto: true,
			minSlides: 4,
			maxSlides: 4,
			slideWidth: 250,
			slideMargin: 10
		});
	});

</script>

<?php
}
add_action('wp_footer', 'wcslider_execute');