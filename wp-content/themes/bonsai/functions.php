<?php
// login style
	function bonsai_login_style(){
		wp_enqueue_style('bonsai_login', get_stylesheet_directory_uri() . '/login/login.css');
	}
	add_action('login_enqueue_scripts', 'bonsai_login_style');

	function bonsai_redirect_login(){
		return home_url();
	}
	add_filter('login_headerurl', 'bonsai_redirect_login');

	function bonsai_new_products($args){
		$args['limit'] = 8;
		$args['columns'] = 4;
		$args['title'] = "Produk Baru";
		return $args;
	}
	add_filter('storefront_recent_products_args', 'bonsai_new_products');

	function bonsai_change_add_to_cart_text(){
		return "Beli";
	}
	add_filter('woocommerce_product_add_to_cart_text', 'bonsai_change_add_to_cart_text');
	add_filter('woocommerce_product_single_add_to_cart_text', 'bonsai_change_add_to_cart_text');


	function bonsai_setup(){
		add_image_size('blog_entry', 400, 257, true);	
	}
	add_action('after_setup_theme', 'bonsai_setup');

	//Add Stylesheets or Sricpts
	function bonsai_scripts(){
		wp_enqueue_style('goole-fonts','https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,900&family=Lora:wght@700&display=swap');
	}
	add_action('wp_enqueue_scripts','bonsai_scripts');


	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price'); 
	add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 35);


	function products_per_page($products)
	{
		$products = 4;
		return $products;
	} 
	add_filter('loop_shop_per_page', 'products_per_page', 20);

	// Remove the homepage content text  and display the feature image

	// function bonsai_homepage_content(){
	// 	remove_action('homepage' ,'storefront_homepage_content');
	// 	add_action('homepage', 'bonsai_homepage_coupon', 10);

	// }
	// add_action('init', 'bonsai_homepage_content');

	// function bonsai_homepage_coupon() {
 //    echo "<div class='main-content'>";
 //    the_post_thumbnail();
 //    echo "</div>";
	// }
// Display Home Kits in the Homepage
	function bonsai_homepage_bonsailangka(){ ?>
	<div class="homepage-bonsai-langka-category">
		<div class="content">
			<div class="columns-3">
				<?php 

					$bonsailangka = get_term(16 ,'product_cat', ARRAY_A); ?>
					<h2 class="section-title"> <?= $bonsailangka['name'];  ?></h2>
					<p><?= $bonsailangka['description']; ?></p>
					<a href=" <?php echo get_category_link($bonsailangka['term_id']); ?>">
						All Product &raquo;
					</a>
			</div>
			<?= do_shortcode('[product_category category="bonsailangka" per_page="4" orderby="price" order="asc" columns="9"]'); ?>
		</div>
	</div>
<?php		
	}
	add_action('homepage', 'bonsai_homepage_bonsailangka', 25);

 /**banner with massage**/

 function bonsai_bonsai2_jepang(){?>
 	<div class="banner-jepang">
 		<div class="columns-4">
 			<h3><?php the_field('banner_text'); ?></h3>
 		</div>
 		<div class="columns-8">	
 				<img src="<?php the_field('banner_image'); ?>" width='668px' height="200px">
 		</div>
 	</div>
<?php 	
 }
 add_action ('homepage', 'bonsai_bonsai2_jepang',80);


/** Print Feature with icons **/

function bonsai_display_features() { ?>
		</main>
		</div><!--#primary--->
			
		</div> <!--.col-full-->
		<div class="home-features">
			<div class="col-full">
				<div class="columns-4">
					<?php the_field('feature_icon_1'); ?>
					<p><?php the_field('feature_content_1'); ?></p>
				</div>
				<div class="columns-4">
					<?php the_field('feature_icon_2'); ?>
					<p><?php the_field('feature_content_2'); ?></p>
				</div>
				<div class="columns-4">
					<?php the_field('feature_icon_3'); ?>
					<p><?php the_field('feature_content_3'); ?></p>
				</div>
			</div>
		</div>

<div class="col-full">
	<div class="content-area">
		<div class="site-main">
			
		

<?php	
}
add_action('homepage', 'bonsai_display_features', 15);

//Display 3 Posts in the homepage

function bonsai_homepage_blog_entries(){ 
	$arg= array(
		'post_type' => 'post',
		'post_per_page' => 3,
		'orderby' => 'date',
		'order' => 'DESC'

	);

	$entries= new WP_Query($arg);
	?>

	
	<div class="homepage-blog-entries">
		<h2 class="section-tittle"> Letest Blog Entries</h2>
		<ul>
			<?php while ($entries->have_posts()): $entries->the_post(); ?>
				<li>
					<?php the_post_thumbnail('blog_entry'); ?>
					<h2 class="entry-tittle"><?php the_title(); ?> </h2>
					<div class="entry-content">
						<header>
							<p>By: <?php the_author(); ?> | <?php the_time(get_option('date_format')); ?></p>
						</header>
						
						<?php 
							$content = wp_trim_words(get_the_content(),20);
							echo "<p>" .$content. "</p>";
						 ?>

						 <a href="<?php the_permalink(); ?>" class="entry-link">Read more &raquo;</a>
						

					</div>
				</li>

			<?php endwhile; wp_reset_postdata(); ?>
		</ul>	
	</div>


<?php
}
add_action('homepage','bonsai_homepage_blog_entries', 90);


function bonsai_display_mailchimp_form(){
if(is_page('welcome')):
    
	?>
		<div class="mailchimp-form">

	 <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
            <div id="mc_embed_signup">
                <form action="//easy-webdev.us11.list-manage.com/subscribe/post?u=b3bb37039b6fbf3db0c1a8331&amp;id=7e7988cfa9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll" class="col-full">
                        <div class="columns-6">
                            <label for="mce-EMAIL">Subscribe to the newsletter <em>access to exclusive deals</em> </label>
                        </div>
                        <div class="columns-6 signup-form">
                                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b3bb37039b6fbf3db0c1a8331_7e7988cfa9" tabindex="-1" value=""></div>
                                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>	
                    </div>
                </form>
            </div>
    </div>
    <?php
    endif;
}

add_action('storefront_before_footer' ,'bonsai_display_mailchimp_form');

// Remove the default woocommerce footer and create a new one!

function bonsai_footer(){
	remove_action('storefront_footer', 'storefront_credit', 20);
	add_action('storefront_after_footer','bonsai_new_footer_text', 20);
}
add_action('init', 'bonsai_footer');

function bonsai_new_footer_text() {
    echo "<div class='reserved'>";
    echo "<p>All Rights Reserved &copy; " . get_bloginfo('name') . " " . get_the_date('Y') . "</p>";
    echo "</div>";
}

// Display Currency in 3 code digit

function bonsai_display_usd($symbol,$currency){

	$symbol = $currency . " $";
	return $symbol; 

}
add_filter('woocommerce_currency_symbol','bonsai_display_usd',10 ,2);

function bonsai_shop_columns($columns) {
    return 4;
}
add_filter('loop_shop_columns', 'bonsai_shop_columns', 20);

// change the  number  of  product per page

// function bonsai_products_per_page($product){

// $product = 4;
// return $product;

// }
// add_filter('loop_shop_per_page', 'bonsai_products_per_page', 20);


// change filter name

function bonsai_new_products_title_filter($orderby ){
	$orderby['date'] = __('New Products First');
	return $orderby;
}
add_filter('woocommerce_catalog_orderby', 'bonsai_new_products_title_filter', 40);

// Display a Placeholder image when no featured image is adde


function bonsai_no_featured_image($image_url){
	 $image_url = get_stylesheet_directory_uri() . '/img/bonsai.jpg';
    return $image_url;
}
add_filter('woocommerce_placeholder_img_src', 'bonsai_no_featured_image');

// Removes a tab in the single page product
// function bonsai_remove_description($tabs) {
//     unset($tabs['description']);
//     return $tabs;
// }
// add_filter('woocommerce_product_tabs', 'bonsai_remove_description', 20);
// Change the Title for the description tab

function bonsai_title_tab_description($tabs) {
    global $post;
    if($tabs['description']):
        $tabs['description']['title'] = $post->post_title;
    endif;  
    return $tabs;
}
add_filter('woocommerce_product_tabs', 'bonsai_title_tab_description', 20);

function bonsai_title_tab_content_description($title) {
    global $post;
    $title = $post->post_title;
    return $title;
}
add_filter('woocommerce_product_description_heading','bonsai_title_tab_content_description' );

function bonsai_display_subtitle_single_product() {
    global $post;
    $subtitle = get_field('subtitle', $post->ID);
    echo "<h3 class='subtitle'>" . $subtitle . "</h3>";
}
add_action('woocommerce_single_product_summary', 'bonsai_display_subtitle_single_product', 6);

// Add a new tab with a video
function bonsai_video_tab($tabs) {
    global $post;
    $video = get_field('video', $post->ID);
    if($video):
        $tabs['video'] = array(
            'title' => 'Video',
            'priority' => 5,
            'callback' => 'bonsai_display_video'
        );
    endif;
    return $tabs;
}
add_filter('woocommerce_product_tabs', 'bonsai_video_tab', 11, 1);

function bonsai_display_video() {
    global $post;
    $video = get_field('video', $post->ID);
    if($video):
        echo '<video controls>';
        echo "<source src='". $video . "'>";
        echo "</video>";
    endif;
}

// Display savings as dollars
// function bonsai_saved_price_dollars($price, $product) {
//     if($product->get_sale_price() ): 
//         $saved = wc_price($product->get_regular_price() - $product->get_sale_price() ); 
//         return $price . sprintf( __('<br> <span class="save-amount"> Save: %s </span>', 'woocommerce' ), $saved );
//     endif;
    
//     return $price;
// }
// add_filter('woocommerce_get_price_html', 'bonsai_saved_price_dollars', 10, 2);

// function bonsai_saved_price_percentage($price, $product) {
//     if($product->get_sale_price() ): 
//         $percentage = round( ( ($product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100);
//         return $price . sprintf( __('<br> <span class="save-amount"> Save: %s </span>', 'woocommerce' ), $percentage . "%" );
//     endif;
//     return $price;
// }
// add_filter('woocommerce_get_price_html', 'bonsai_saved_price_percentage', 10, 2);


function bonsai_display_savings($price, $product) {
    if($product->get_sale_price() ): 
        $regular_price = $product->get_regular_price();
        
        if($regular_price > 100) {
            $saved = wc_price($product->get_regular_price() - $product->get_sale_price() ); 
            return $price . sprintf( __('<br> <span class="save-amount"> Save: %s </span>', 'woocommerce' ), $saved );
        } else {
            $percentage = round( ( ($product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100);
            return $price . sprintf( __('<br> <span class="save-amount"> Save: %s </span>', 'woocommerce' ), $percentage . "%" );
        }
    endif;
    return $price;
}
add_filter('woocommerce_get_price_html', 'bonsai_display_savings', 10, 2);

/** Print Social Sharing Icons **/
function bonsai_display_sharing_buttons() { ?>
    <div class="addthis_inline_share_toolbox"></div>
<?php
}
add_action('woocommerce_before_add_to_cart_form', 'bonsai_display_sharing_buttons');

function bonsai_include_addthis_scripts() { ?>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f1d73a1251e5c9a"></script> 
<?php
}
add_action('wp_footer', 'bonsai_include_addthis_scripts');

/* Display banner in the cart page*/
function bonsai_display_banner_cart_page() {
    global $post;
    $image_url = get_field('banner', $post->ID);
    if($image_url): ?>
        <div class="coupon-cart">
            <img src="<?php echo $image_url ?>" alt="coupon">
        </div>
        
    <?php endif;
}
add_action('woocommerce_check_cart_items', 'bonsai_display_banner_cart_page');

// Display a button tu clear the cart

function bonsai_empty_cart_button() { ?>
    <a class="button" href="?empty-cart=true">Empty Cart</a>
<?php    
}
add_action('woocommerce_cart_actions', 'bonsai_empty_cart_button');

function bonsai_empty_cart() {
    if(isset($_GET['empty-cart'])):
        global $woocommerce;
        $woocommerce->cart->empty_cart();
    endif;
}
add_action('init', 'bonsai_empty_cart');

// Remove the Phone Field from Checkout
function bonsai_checkout_fields($fields) {
    unset($fields['billing']['billing_phone']);
    $fields['billing']['billing_email']['class'] = array('form-row-wide');
    
    return $fields;
}
add_filter('woocommerce_checkout_fields', 'bonsai_checkout_fields', 20);

// Add extra fields to checkout

function bonsai_add_checkout_fields($fields) {

    $fields['billing']['itin'] = array(
        'css' => array('form-row-wide'),
        'label' => 'ITIN (Individual TaxPayer Identification Number)',
        'required' => true
    );
    $fields['order']['heard_about_us'] = array(
        'type' => 'select',
        'css' => array('form-row-wide'),
        'label' => 'How did you hear about us?',
        'options' => array(
            'default' => 'Choose...',
            'tv'    => 'Television',
            'radio' => 'Radio',
            'newspaper' => 'Newspaper',
            'internet'  => 'Internet',
            'facebook'  => 'Facebook'
        )
    );
    return $fields;
}
add_filter('woocommerce_checkout_fields', 'bonsai_add_checkout_fields', 20);


/* Related Products in Blog*/
function bonsai_blog_related_products() {
    global $post;
    $related_products = get_field('related_products' ,$post->ID);
    
    if($related_products):
        $product_ids = join($related_products, ', '); ?>
        <div class="related-products">
            <h2 class="section-title">Related Products</h2>
            <?php echo do_shortcode('[products ids="'.$product_ids.'" columns="8"]') ?>
        </div>
    <?php endif;
}
add_action('storefront_post_content_after', 'bonsai_blog_related_products');
